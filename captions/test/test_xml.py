from xml.etree.ElementTree import Element, SubElement, Comment
from xml.etree import ElementTree
from xml.dom import minidom
import xml.etree.cElementTree as ET

def prettify(elem):
    """ Return a pretty-printed XML string for the Element """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def createXML(image_id, image_captions):
    ''' Create XML file with captions for a specific id ''' 
    readable = Element('readable')

    title = SubElement(readable, 'title')
    title.text = image_id

    content = SubElement(readable, 'content')
    content.text = image_captions
    
    print '\n', prettify(readable)
    tree = ET.ElementTree(readable)
    tree.write('temp.xml')

def parseCaptions():
    ''' Run Program '''
    image_id = 'insert title here'
    image_captions = '\nA child in a pink dress is climbing up a set of stairs in an entry way.\nA girl going into a wooden building.\nA little girl climbing into a wooden playhouse.\nA little girl climbing the stairs to her playhouse.\nA little girl in a pink dress going into a wooden cabin.'
    createXML(image_id, image_captions)

parseCaptions()
